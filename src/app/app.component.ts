import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'blog';

  posts = [
  {
      title: 'Mon Premier Post',
      content: 'lalialaaiaia',
      loveIts: 0,
      created_at: Date
    },
    {
      title: 'Mon deuxième post',
      content: 'lalalalalala',
      loveIts: 0,
      created_at: Date
    },
    {
      title: 'Encore un post cest le troisieme',
      content: 'lalalalalal',
      loveIts: 0,
      created_at: Date
    }
  ];
}
