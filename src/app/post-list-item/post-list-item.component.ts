import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {
  @Input() postName: string;
  @Input() postLikeIt: number;
  @Input() postContent: string;

  datePost = new Date();

  constructor() { }

  ngOnInit() {
  }

  getColor(){
    if(this.postLikeIt > 0) {
      return 'green';
    } else if(this.postLikeIt < 0) {
      return 'red';
    }
  }

  likeIt(){
    this.postLikeIt=this.postLikeIt + 1
    return this.postLikeIt ;
  }


  dontLikeIt(){
    this.postLikeIt=this.postLikeIt - 1
    return this.postLikeIt ;
  }

}
